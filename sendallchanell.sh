cerceyn="╭──────────────────────────────────────────╮\n"
cerceyn+= "g\033[31;40;5m"⠀⠀⠀⠀⠀⠀⠀⠀⠀
cerceyn+="│⠀⠀⠀⠀⠀⠀⠀█▀▀ █▀▀ █▀█ █▀▀ █▀▀ █▄█ █▄░█⠀⠀⠀⠀⠀⠀⠀│"
cerceyn+="\n│⠀⠀⠀⠀⠀⠀⠀█▄▄ ██▄ █▀▄ █▄▄ ██▄ ░█░ █░▀█⠀⠀⠀⠀⠀⠀⠀│\033[0m"
cerceyn+="\n╰──────────────────────────────────────────╯"
cerceyn+="\n\n\033[31;40;5m│⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀✨Powered by @cerceyn✨⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀│\n\033[0m"
MESAJ="\n\033[38;40;1m          📱 sendallchannel 📱"
MESAJ+="\n "
MESAJ+="\n          ❗İşlem Bitene Kadar Uygulamayı Terk Etmeyin❗\033[0m"
MESAJ+="\n───────────────────────────────────────────────────────\n"
YARDIM+="\n "
BOSLUK="\n "
clear
echo -e $cerceyn
sleep 4
echo -e $YARDIM
echo -e $BOSLUK
echo -e "\033[35;40;1m⏳ Gereksinimleri Güncelliyorum ⏳"
echo -e "⏳ Update Your Requirements ⏳\033[4m"
echo -e $BOSLUK
sleep 2
pkg update -y
clear
echo -e $cerceyn
echo -e $BOSLUK
echo -e $MESAJ
echo -e $BOSLUK
echo -e "\033[35;40;1m⌛ Cihazınıza Python Kuruluyor.. ⌛"
echo -e "⌛ Python is Installed On Your Device ⌛\033[0m"
sleep 2
echo -e $BOSLUK
pkg install python -y
pip install --upgrade pip
clear
echo -e $cerceyn
echo -e $MESAJ
echo -e $BOSLUK
echo -e "⌛ Git Kuruluyor ⌛"
echo -e "⌛ Installing Git ⌛"
echo -e $BOSLUK
sleep 2
pkg install git -y
clear
echo -e $cerceyn
echo -e $MESAJ
echo -e $BOSLUK
echo -e "\033[35;40;1m⌛ Tüm Kodların Son Sürümünü Indiriyorum ⌛"
echo -e "⌛ Downloading All Files (Latest Version) ⌛\033[0m"
echo -e $BOSLUK
sleep 2
git clone https://github.com/cerceyn/sendallchannel.git
clear
echo -e $cerceyn
echo -e $BOSLUK
echo -e $MESAJ
echo -e $BOSLUK
echo -e "\033[33;40;1m 📁 İlgili Klasöre Geçiyorum..\033[0m"
sleep 1
cd sendallchannel
echo -e " \033[35;40;1m📥 İlgili Gereksinimler Kuruluyor..."
echo -e " 📥 Installing Requirements...\033[0m"
echo -e $BOSLUK
sleep 2
pip install -r requirements.txt
clear
echo -e $cerceyn
echo -e $YARDIM
echo -e $BOSLUK
echo -e "\033[32;40;1m 📥 Program Başlatılıyor..."
echo -e " 📥 Starting Program...\033[0m"
sleep 4
echo 
python -m android



